﻿CREATE TABLE employee (
	id integer NOT NULL PRIMARY KEY,
	name text,
	salary integer,
	manager_id integer,
	department_id integer
)

INSERT INTO employee (id, name, salary)
values (1, 'Vasya', 10), (2, 'Petya', 20), (3, 'Vanya', 30), (4, 'Roma', 40), (5, 'Sasha', 50);

SELECT salary FROM employee
WHERE name = 'Vasya'

UPDATE employee SET  salary = 20
WHERE name = 'Vasya'

DELETE FROM employee WHERE name = 'Petya'

ALTER TABLE employee add column xxx integer

INSERT INTO employee (id, name, salary)
values (6, 'Slaanesh', 400), (7, 'Khorn', 500);

UPDATE employee SET department_id = 1
WHERE name in('Vanya', 'Vasya', 'Roma', 'Sasha')

UPDATE employee SET department_id = 0
WHERE name in('Slaanesh', 'Khorn')

SELECT AVG(salary), department_id FROM employee 
GROUP BY department_id 
HAVING AVG(salary) > 10

ALTER TABLE employee ADD CONSTRAINT manager_employee_fk FOREIGN KEY (manager_id) REFERENCES employee (id)

UPDATE employee SET manager_id = 6 WHERE name in('Vanya', 'Vasya')

UPDATE employee SET manager_id = 7 WHERE name in('Roma', 'Sasha')

SELECT employee.name FROM employee
JOIN employee AS manager ON employee.manager_id = manager.id
WHERE employee.salary > manager.salary

SELECT * FROM employee
JOIN (SELECT department_id, MAX(salary) FROM employee
GROUP BY department_id) as q on employee.department_id = q.department_id
WHERE employee.salary = q.max
