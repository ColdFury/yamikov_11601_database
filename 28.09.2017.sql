﻿CREATE TABLE department (
	id serial NOT NULL PRIMARY KEY,
	name text
)


INSERT INTO department(name) VALUES ('First'), ('Second')


ALTER TABLE employee ADD CONSTRAINT department_fk FOREIGN KEY (department_id) REFERENCES department (id)


SELECT department.name FROM department
JOIN employee ON department.id = employee.department_id
GROUP BY department.id
HAVING count(department.id) <= 4


SELECT department.name FROM department
JOIN employee ON department.id = employee.department_id
GROUP BY department.id
HAVING sum(salary) = (
	SELECT max(sum) FROM (
		SELECT sum(salary) FROM department
		JOIN employee ON department.id = employee.department_id
		GROUP BY department.id
	) AS summa
)


SELECT employee.name FROM employee
JOIN employee AS manager ON employee.manager_id = manager.id
WHERE employee.department_id != manager.department_id


SELECT * FROM employee
FULL OUTER JOIN employee AS manager ON employee.manager_id = manager.id
EXCEPT (
	SELECT * FROM employee
	INNER JOIN employee AS manager ON employee.manager_id = manager.id
)