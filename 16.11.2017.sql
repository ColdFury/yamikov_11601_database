﻿CREATE OR REPLACE FUNCTION fib(num INTEGER, OUT result INTEGER)
AS
$$
BEGIN
  IF num > 2
  THEN result := fib(num - 1) + fib(num - 2);
    ELSE result := 1;
  END IF;
END
$$
LANGUAGE plpgsql;

SELECT fib(10);

*****************************************************************************************

CREATE OR REPLACE FUNCTION bubble_sort(arr INTEGER [], OUT result INTEGER [])
AS
$$
DECLARE a INTEGER;
BEGIN
  FOR i IN 1..(array_length(arr, 1) - 1) LOOP
    FOR j IN REVERSE i..2 LOOP
      IF (arr [j] < arr [j - 1])
      THEN
        a := arr [j];
        arr [j] := arr [j - 1];
        arr [j - 1] := a;
      END IF;
    END LOOP;
  END LOOP;
  result := arr;
END
$$
LANGUAGE plpgsql;

SELECT *
FROM bubble_sort(ARRAY [5, -5, 4, 3, 1, 2, 10, 8, 0, 15]);