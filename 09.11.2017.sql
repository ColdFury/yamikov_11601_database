﻿CREATE TABLE animals (
  id SERIAL NOT NULL PRIMARY KEY,
  type VARCHAR(10) NOT NULL,
  color VARCHAR(10)
)


INSERT INTO animals (type, color) VALUES ('dog', 'black'), ('cat', 'gray'), ('dog', 'white'), ('fox', 'red'), ('dog', 'black')


ALTER TABLE animals ADD COLUMN count INTEGER;
WITH counts AS (SELECT
                  type,
                  color,
                  count(*) AS count
                FROM animals
                GROUP BY type, color)
UPDATE animals
SET count = counts.count
FROM counts
WHERE animals.type = counts.type AND animals.color = counts.color


DELETE FROM animals
WHERE id NOT IN (
	SELECT min(id) FROM animals
	GROUP BY type, color)

SELECT * FROM animals


*******************************************************************************************


CREATE TABLE input (
  clusterID VARCHAR(10) NOT NULL PRIMARY KEY
)


CREATE TABLE clusters (
  clusterID VARCHAR(10) NOT NULL PRIMARY KEY,
  date DATE NOT NULL
)

CREATE TABLE deleted_nodes (
  clusterID VARCHAR(10) NOT NULL PRIMARY KEY,
  date DATE NOT NULL
)

INSERT INTO input (clusterID) VALUES ('cluster1'), ('cluster2'), ('cluster3')

DELETE FROM input;
INSERT INTO input (clusterID) VALUES ('cluster1')

DELETE FROM input;
INSERT INTO input (clusterID) VALUES ('cluster1'), ('cluster2'), ('cluster3'), ('cluster4')


INSERT INTO deleted_nodes(clusterID, date)
SELECT clusters.clusterID, CURRENT_DATE FROM clusters
WHERE clusters.clusterID NOT IN (SELECT input.clusterID FROM input);
DELETE FROM deleted_nodes
WHERE deleted_nodes.clusterID IN (SELECT input.clusterID FROM input);
INSERT INTO clusters(clusterID, date)
SELECT input.clusterID, CURRENT_DATE FROM input
WHERE input.clusterID NOT IN (SELECT clusters.clusterID FROM clusters);
DELETE FROM clusters
WHERE clusters.clusterID NOT IN (SELECT input.clusterID FROM input);

SELECT * FROM clusters
WHERE date > CURRENT_DATE - 10

SELECT * FROM deleted_nodes
ORDER BY date ASC LIMIT 10

SELECT * FROM input
SELECT * FROM clusters
SELECT * FROM deleted_nodes